# CS:GO/CS:S AutoStrafe
A CS:GO/CS:S AutoStrafe plugin for Sourcemod. Admins with the CHEAT flag can use !sm_autostrafe to enable an autostrafer. This will cause them to simulate a strafe hack whenever they jump, useful for minigames servers, bunnyhop etc. 

I'm not responsible for any anti-cheat this sets off, any bans you end up giving yourself or anything of the kind. Please note that this uses logic from cheating software and will therefore be detected by most SM anti-cheats. Use at your own caution.

# How to use
Drop the `sm_autostrafe.smx` into the `addons/sourcemod/plugins` folder and reload the map. From there, any admin with the cheats flag will be able to do `!autostrafe` to toggle the autostrafe.

# Any improvements?
Feel free to submit issues/pull requests. I cannot ensure that I will update the plugin, but I'll definitely have a look.